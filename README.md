Product screenshots for KDE software.

# Publishing

- Git @ `kde:websites/product-screenshots`
- Published @ `https://cdn.kde.org/screenshots/`

Publishing can take an hour or three.

When you have multiple screenshots please create a subdirectory for
your application, also do this if you expect you may want additional
screenshots in the future.

Before pushing please `optipng` your screenshots to make sure they aren't unnecessarily large.

Please note that the CDN only updates on a schedule (> 1 hour).

# Guidelines

Generally you should follow https://docs.flathub.org/docs/for-app-authors/metainfo-guidelines/quality-guidelines/#screenshots when creating screenshots.

In particular:
 - Screenshots should be taken on a recent Plasma, with default (light) style and window decoration
 - Screenshots should show default settings
 - Screenshots should be in English
 - Screenshots should not show obvious bugs, e.g. missing titlebar icons

When creating screenshots for appstream please also check out the appstream documentation:
https://www.freedesktop.org/software/appstream/docs/chap-Metadata.html#tag-screenshots

